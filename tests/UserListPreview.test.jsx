import UserListPreview from '../src/components/UserListPreview';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('UserListPreview', () => {
  it('should render', () => {
    shallow(
      <UserListPreview
        users={[]}
        count={10}
      />
    );
  });

  it('should render at most three users', () => {
    const sampleUsers = [
      {
        id : 123,
        images: {},
        username: 'somebody'
      },
      {
        id : 124,
        images: {},
        username: 'somebody 2'
      },
      {
        id : 125,
        images: {},
        username: 'somebody 3'
      },
      {
        id : 126,
        images: {},
        username: 'somebody 4'
      },
    ];

    const userListPreview = shallow(<UserListPreview users={sampleUsers} />);
    expect(userListPreview.find('.user-link').length).toEqual(3);
  });

  it('should render correct count', () => {
    const userListPreview = shallow(<UserListPreview users={[]} count={1200} />);
    expect(userListPreview.find('.user-count').text()).toBe('1200');
  });

  it('should render 0 for empty count prop', () => {
    const userListPreview = shallow(<UserListPreview users={[]} count={null} />);
    expect(userListPreview.find('.user-count').text()).toBe('0');
  });
});
