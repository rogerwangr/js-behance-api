import ProjectList from '../src/components/ProjectList';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('ProjectList', () => {
  it('should render', () => {
    shallow(
      <ProjectList
        projects={[]}
      />
    );
  });

  it('should render right number of projects', () => {
    const sampleProjects = [
      {
        id: 123,
        covers: {
          115: ''
        },
        name: 'A very artistic piece',
        stats: {
          views: 12,
          appreciations: 0
        }
      },
      {
        id: 124,
        covers: {
          115: ''
        },
        name: 'Wow',
        stats: {
          views: 40203,
          appreciations: 1402
        }
      }
    ];

    const projectList = shallow(<ProjectList projects={sampleProjects} />);

    expect(projectList.find('.project').length).toEqual(2);
  });
});
