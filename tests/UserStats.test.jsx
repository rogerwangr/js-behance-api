import UserStats from '../src/components/UserStats';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('UserStats', () => {
  it('should render', () => {
    shallow(
      <UserStats
        stats={{}}
        followers={[]}
        following={[]}
      />
    );
  });

  it('should render the right numbers', () => {
    const sampleStats = {
      views: 12,
      appreciations: 123,
      followers: 1234,
      following: 12345,
    };

    const userStats = shallow(
      <UserStats
        stats={sampleStats}
        followers={[]}
        following={[]}
      />
    );

    const appreciationsCount = userStats.find('#appreciations .count').text();
    expect(appreciationsCount).toBe('123');

    const viewsCount = userStats.find('#views .count').text();
    expect(viewsCount).toBe('12');
  });
});
