import UserProfile from '../src/components/UserProfile';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('UserProfile', () => {
  const initialProps = {
    params: {
      name:'s-d-g'
    }
  };

  const newProps = {
    match: {
      params: {
        name:'someone-else'
      }
    }
  };

  it('should render', () => {
    shallow(<UserProfile match={initialProps} />);
  });

  it('should call getData on mount', () => {
    const spy = jest.spyOn(UserProfile.prototype, 'getData');
    const userProfile = shallow(<UserProfile match={initialProps} />);
    expect(spy).toHaveBeenCalled();
  });

  it('should call getData on update', () => {
    const spy = jest.spyOn(UserProfile.prototype, 'getData');
    const userProfile = shallow(<UserProfile match={initialProps} />);
    expect(spy.mock.calls.length).toBe(1);

    userProfile.setProps(newProps);
    expect(spy.mock.calls.length).toBe(2);
  });

  it('should change state.username through getDerivedStateFromProps', () => {
    const spy = jest.spyOn(UserProfile.prototype, 'getData');
    const userProfile = shallow(<UserProfile match={initialProps} />);

    expect(UserProfile.getDerivedStateFromProps(newProps, userProfile.state()).username).toBe('someone-else');

    userProfile.setProps(newProps); // update component
    expect(userProfile.state().username).toBe('someone-else');
  });
});
