import WorkExperienceList from '../src/components/WorkExperienceList';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('WorkExperienceList', () => {
  it('should render', () => {
    shallow(
      <WorkExperienceList
        workExperience={[]}
      />
    );
  });

  it('should render right number of work experience items', () => {
    const sampleWorkExperience = [
      {
        "position": "Designer",
        "organization": "Freelance",
        "location": ""
      },
      {
        "position": "Construction worker",
        "organization": "Yellow hat co.",
        "location": ""
      }
    ];

    const workExperienceList = shallow(<WorkExperienceList workExperience={sampleWorkExperience} />);

    expect(workExperienceList.find('.work-experience').length).toEqual(2);
  });
});
