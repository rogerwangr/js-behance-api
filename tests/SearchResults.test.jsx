import SearchResults from '../src/components/SearchResults';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('SearchResults', () => {

  it('should render', () => {
    shallow(
      <SearchResults
        results={[]}
      />
    );
  });

  it('should call render when results change', () => {
    const spy = jest.spyOn(SearchResults.prototype, 'render');
    const newResults = {
      results: [
        {
          username: 'something',
          display_name: 'some thing',
          images: {
            100: ''
          }
        },
      ]
    };

    const searchResults = shallow(<SearchResults results={[]} />);
    expect(spy.mock.calls.length).toBe(1);

    searchResults.setProps(newResults);

    expect(spy.mock.calls.length).toBe(2);
  });
});
