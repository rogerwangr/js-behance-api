import SearchBox from '../src/components/SearchBox';
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

describe('SearchBox', () => {
  const sampleUserResult = {
    'users':[
      {
        'id': 1234,
        'first_name': 'Someone',
        'last_name': 'Cool ',
        'username': 'someone-cool',
        'city': 'Place',
        'state': '',
        'country': 'Albania',
        'location': 'Tirana, Albania',
        'company': 'No Name Company',
        'occupation': 'Visual Designer',
        'created_on': 1215524927,
        'url': 'https://www.behance.net/someone-cool',
        'images': {
          '50': '',
          '100':'',
          '115': '',
          '138': '',
          '230': '',
          '276': ''
        },
        'display_name': 'Someone Cool',
        'fields': ['Graphic Design','Art Direction','Print Design'],
        'has_default_image': 0,
        'website':'',
        'stats': {
          'followers':27912,
          'following':390,
          'appreciations':39289,
          'views':375895,
          'comments':83
        }
      }
    ],
    "http_code":200
  };

  const newResult = {
    'users':[
      {
        'id': 777,
        'first_name': 'Tim',
        'last_name': 'Duncan ',
        'username': 'tim-dunkin',
        'city': 'Place',
        'state': '',
        'country': 'Albania',
        'location': 'Tirana, Albania',
        'company': 'No Name Company',
        'occupation': 'Visual Designer',
        'created_on': 1215524927,
        'url': 'https://www.behance.net/someone-cool',
        'images': {
          '50': '',
          '100':'',
          '115': '',
          '138': '',
          '230': '',
          '276': ''
        },
        'display_name': 'Tim Duncan',
        'fields': ['Graphic Design','Art Direction','Print Design'],
        'has_default_image': 0,
        'website':'',
        'stats': {
          'followers':27912,
          'following':390,
          'appreciations':39289,
          'views':375895,
          'comments':83
        }
      }
    ],
    "http_code":200
  };

  it('should render', () => {
    shallow(<SearchBox />);
  });

  it('should generate the right URL', () => {
    const searchBox = shallow(<SearchBox />);
    const instance = searchBox.instance();
    expect(instance.generateURL('something')).toBe(`/api/users?q=something`);
    expect(instance.generateURL('some thing')).toBe(`/api/users?q=some%20thing`);
  });

  it('should call handleChange when input changes', () => {
    const spy = jest.spyOn(SearchBox.prototype, 'handleChange');
    const searchBox = shallow(<SearchBox />);
    searchBox.find('input').simulate('change');
    expect(spy).toHaveBeenCalled();
  });

  it('should get data and set state on mount', async () => {
    fetch.resetMocks();
    fetch.mockResponse(JSON.stringify(sampleUserResult));
    const searchBox = shallow(<SearchBox />);
    expect(searchBox.state().searchResults.length).toEqual(0); // before fetch

    await searchBox.instance().componentDidMount();

    expect(searchBox.state().searchResults.length).toEqual(1);
    expect(searchBox.state().searchResults[0].id).toEqual(1234);
  });

  it('should get data and set state on change', async () => {
    fetch.resetMocks();
    fetch.mockResponse(JSON.stringify(sampleUserResult));
    const searchBox = shallow(<SearchBox />);

    await searchBox.instance().componentDidMount();
    expect(searchBox.state().searchResults[0].id).toEqual(1234);

    fetch.resetMocks();
    fetch.mockResponseOnce(JSON.stringify(newResult));
    const fakeEvent = {target: {value: ''}};
    await searchBox.instance().handleChange(fakeEvent);
    expect(searchBox.state().searchResults[0].id).toEqual(777);

  });
});
