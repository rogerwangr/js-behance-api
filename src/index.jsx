import App from './components/App.jsx';
import {
  BrowserRouter as Router,
} from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';

require('./styles/main.scss');

ReactDOM.render((
  <Router>
    <App />
  </Router>
), document.getElementById('react-app'));
