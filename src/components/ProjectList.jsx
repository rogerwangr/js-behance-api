import PropTypes from 'prop-types';
import React from 'react';

const ProjectList = (props) => {
  const projects = props.projects;

  if (projects.length === 0) {
    return null;
  }

  const projectList = projects.map((project) => {
    return (
      <li className='project' key={project.id}>
        <a href={project.url} target='_blank'>
          <img src={project.covers[115]}/>
          <div className='project-details'>
            <div className='project-name'>{project.name}</div>
            <div className='project-stats'>
              <div>Views: {project.stats.views}</div>
              <div>Appreciations: {project.stats.appreciations}</div>
            </div>
          </div>
        </a>
      </li>
    );
  });

  return (
    <div id='project-list'>
      <h3>Projects</h3>
      <ul>
        {projectList}
      </ul>
    </div>
  );
};

ProjectList.propTypes = {
  // Array of objects containing project information
  projects: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      url: PropTypes.string,
      covers: PropTypes.objectOf(PropTypes.string), // object containing URL links to images
      name: PropTypes.string
    })
  )
};

export default ProjectList;
