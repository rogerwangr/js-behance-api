import { Link } from 'react-router-dom';
import React from 'react';

const Header = () => {
  return (
    <div id='header'>
      <h1>
        <Link to='/'>
          Behance Portfolios
        </Link>
      </h1>
    </div>
  );
};

export default Header;
