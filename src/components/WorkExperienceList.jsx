import PropTypes from 'prop-types';
import React from 'react';

const WorkExperienceList = (props) => {
  const workExperiences = props.workExperience;

  if (props.workExperience.length === 0) {
    return null;
  }

  const workList = workExperiences.map((workExperience, index) => {
    return (
      <li className='work-experience' key={index}>
        <span className='position'>{workExperience.position}</span>
        <span className='divider'>|</span>
        <span className='organization'>{workExperience.organization}</span>
      </li>
    );
  });

  return (
    <div id='work-experience-list'>
      <h3>Work Experience</h3>
      <ul>
        {workList}
      </ul>
    </div>
  );
};

WorkExperienceList.propTypes = {
  // Array of objects containing work experience data
  workExperience: PropTypes.arrayOf(
    PropTypes.shape({
      position: PropTypes.string,
      organization: PropTypes.string
    })
  )
};

export default WorkExperienceList;
