import React from 'react';
import SearchResults from './SearchResults.jsx';

export default class SearchBox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchResults: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.generateURL = this.generateURL.bind(this);
  }

  // I could take this out and put it into a utils.js folder, but for this
  // project, nothing else is going to use this function
  generateURL(searchText) {
    const queryText = encodeURIComponent(searchText);
    return `/api/users?q=${queryText}`;
  }

  async searchForUsers(query='') {
    const searchResults = await fetch(this.generateURL(query));
    return searchResults ? await searchResults.json() : {users: []};
  }

  async handleChange(evt) {
    const searchQuery = evt.target.value;
    const results = await this.searchForUsers(searchQuery);

    this.setState({
      searchResults: results.users
    });
  }

  async componentDidMount() {
    const results = await this.searchForUsers();

    this.setState({
      searchResults: results.users
    });
  }

  render() {
    return (
      <div id='search-box'>
        <input
          type='text'
          placeholder='search for Behance users...'
          onChange={this.handleChange}
        />
        <SearchResults results={this.state.searchResults}/>
      </div>
    );
  }
}
