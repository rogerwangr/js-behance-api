import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import React from 'react';

const UserListPreview = (props) => {
  const users = props.users;

  let classes = 'user-list-preview';

  if (props.users.length === 0) {
    classes += ' none';
  }

  const userDisplays = [];
  // Only display 3 users in preview
  for (let i = 0; i < users.length && i < 3; i++) {
    const user = users[i];
    const userPath = `/user/${user.username}`;
    userDisplays.push(
      <li key={user.id}>
        <Link to={userPath}>
          <img src={user.images[50]} />
          <span className='user-link'>#{user.username}</span>
        </Link>
      </li>
    );
  }

  return (
    <div className={classes}>
      <span className='user-count'>{props.count || '0'}</span>
      <ul>
        {userDisplays}
      </ul>
    </div>
  );
};

UserListPreview.propTypes = {
  // Array of objects containing project information
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      images: PropTypes.objectOf(PropTypes.string), // object containing URL links to images
      username: PropTypes.string
    })
  ),

  // Total number of users in list (e.g. total followers/following)
  count: PropTypes.number
}

export default UserListPreview;
