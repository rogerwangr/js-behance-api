import Header from './Header.jsx';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SearchBox from './SearchBox.jsx';
import UserProfile from './UserProfile.jsx';

const App = () => {
  return (
    <div id='app'>
      <Header />
      <Switch>
        <Route exact path='/' component={SearchBox} />
        <Route path='/user/:name' component={UserProfile} />
      </Switch>

    </div>
  );
};

export default App;
