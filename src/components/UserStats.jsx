import PropTypes from 'prop-types';
import React from 'react';
import UserListPreview from './UserListPreview.jsx';

const UserStats = (props) => {
  if (!props.stats || !props.stats.views) {
    return null;
  }

  return (
    <div id='user-stats'>
      <div id='appreciations' className='stat'>
        <label>Appreciations</label>
        <span className='count'>{props.stats.appreciations}</span>
      </div>

      <div id='views' className='stat'>
        <label>Views</label>
        <span className='count'>{props.stats.views}</span>
      </div>

      <div id='followers' className='stat'>
        <label>Followers</label>
        <UserListPreview
          users={props.followers}
          count={props.stats.followers}
        />
      </div>

      <div id='following' className='stat'>
        <label>Following</label>
        <UserListPreview
          users={props.following}
          count={props.stats.following}
        />
      </div>
    </div>
  );
};

UserStats.propTypes = {
  // Object containing stats about the user
  stats: PropTypes.shape({
    appreciations: PropTypes.number,
    views: PropTypes.number,
    followers: PropTypes.number,
    following: PropTypes.number,
  }),

  // Array containing basic user data for followers of this user
  followers: PropTypes.array,

  // Array containing basic user data for users this user follows
  following: PropTypes.array
};

export default UserStats;
