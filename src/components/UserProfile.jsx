import React from 'react';
import ProjectList from './ProjectList.jsx';
import UserStats from './UserStats.jsx';
import WorkExperienceList from './WorkExperienceList.jsx';

export default class UserProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      followers: [],
      following: [],
      projects: [],
      workExperience: [],
      username: this.props.match.params.name // match URL path from Route
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.match.params.name !== prevState.username){
      return { username: nextProps.match.params.name };
    }

    return null;
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.name !== this.state.username) {
      this.getData();
    }
  }

  async getData() {
    const username = this.state.username;

    const jsonifyData = (data) => data.json();
    const asyncRequests = [
      fetch(`/api/users/${username}`).then(jsonifyData),
      fetch(`/api/users/${username}/followers`).then(jsonifyData),
      fetch(`/api/users/${username}/following`).then(jsonifyData),
      fetch(`/api/users/${username}/projects`).then(jsonifyData),
      fetch(`/api/users/${username}/work_experience`).then(jsonifyData)
    ];

    const allData = await Promise.all(asyncRequests);

    this.setState({
      user: allData[0].user,
      followers: allData[1].followers || [],
      following: allData[2].following || [],
      projects: allData[3].projects || [],
      workExperience: allData[4].work_experience || []
    });
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    const user = this.state.user;

    return (
      <div id='user-profile'>
        <div className='user-name-section'>
          <img id='user-image' src={user.images ? user.images[138] : null} />
          <h2 id='display-name'>{user.display_name}</h2>
          <span id='user-name'>#{user.username}</span>
        </div>
        <WorkExperienceList workExperience={this.state.workExperience} />
        <hr></hr>
        <UserStats
          stats={user.stats}
          followers={this.state.followers}
          following={this.state.following}
        />
        <hr></hr>
        <ProjectList projects={this.state.projects} />
      </div>
    );
  }
};
