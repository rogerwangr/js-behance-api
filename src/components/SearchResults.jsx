import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom'

export default class SearchResults extends React.Component {
  constructor(props) {
    super(props);

    this.renderResults = this.renderResults.bind(this);
  }

  renderResults() {
    const resultsList = this.props.results.map(result => {
      const userPath = `/user/${result.username}`;
      return (
        <li className='user-result' key={result.id}>
          <Link to={userPath}>
            <img src={result.images[100]} />
            <div className='display-name'>
              {result.display_name}
            </div>
            <div className='user-name'>
              #{result.username}
            </div>
          </Link>
        </li>
      );
    });

    return resultsList;
  }

  render() {
    return (
      <div id='search-results'>
        <ul>
          {this.renderResults()}
        </ul>
      </div>
    );
  }
}

SearchResults.propTypes = {
  // Array of objects containing project information
  results: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      display_name: PropTypes.string,
      username: PropTypes.string,
      images: PropTypes.objectOf(PropTypes.string) // object containing URL links to images
    })
  )
};
