const express = require('express');
const fetch = require('node-fetch');
const API = require('./local.js').API;

const server = express();

server.get('/users*', async function(req, res) {
  let url = `https://www.behance.net/v2${req.path}?api_key=${API}`;

  if (req.query.q) {
    url = url.concat(`&q=${encodeURIComponent(req.query.q)}`);
  }

  try {
    const data = await fetch(url);
    const jsonData = await data.json();

    res.json(jsonData);
  } catch (e) {
    res.status(500);
  }
});

server.listen(1337);
